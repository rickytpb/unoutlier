from setuptools import setup, find_packages
from os import path

NAME = 'unoutlier'
ver_file = path.join('src',NAME, 'version.py')
with open(ver_file) as f:
    exec(f.read())

setup(
    name = NAME, 
    version=__version__, 
    package_dir={'':'src'},
    packages=find_packages(),
)