import pandas as pd
import numpy as np
from scipy.stats import norm, t
from unoutlier.utils import column_name_to_index


def __calculate_z(df, col_idx, mean=False, stdev=False):
    vals = df.iloc[:,col_idx]
    if not mean:
        mean = np.mean(vals)
    if not stdev:
        stdev = np.sqrt(np.var(vals))
    z_score = (vals - mean)/stdev
    return z_score

def __calculate_mad(df,col_idx):
    vals = df.iloc[:,col_idx]
    mad = np.median(np.absolute(vals - np.median(vals)))
    mad_score = (0.6745*(vals-np.mean(vals)))/mad 
    return mad_score 

def z_score(df,col,conf_level=0.05,dist='norm',mean=False,stdev=False):
    col_index = column_name_to_index(df, col)
    z_score = __calculate_z(df,col_index,mean,stdev)
    if dist=='norm':
        conf = norm.ppf(conf_level/2)
    elif dist=='t':
        conf = t.ppf(conf_level,len(df.index)-1)
    else:
        raise AttributeError('Please select a valid distribution: "norm","t"')
    if conf_level>1:
        conf = conf_level
        return df.loc[(z_score < -conf) | (z_score > conf)].index.to_list()
    return df.loc[(z_score >= -conf) | (z_score <= conf)].index.to_list()

def mad_score(df,col, conf_level=3.5):
    col_index = column_name_to_index(df, col)
    mad_score =  __calculate_mad(df,col_index)
    return df.loc[(mad_score < -conf_level) | (mad_score > conf_level)].index.to_list()

def tukey_iqr(df, col, coeff=1.5):
    col_index = column_name_to_index(df, col)
    q1,q3 = np.percentile(df.iloc[:,col_index], [25,75])
    iqr = q3-q1
    df_result = df.loc[(df.iloc[:,col_index] >= q3+(coeff*iqr)) | (df.iloc[:,col_index] <= q1-(coeff*iqr))]
    return df_result.index.tolist()

def __calculate_esd_test_stat(vals):
    stdev = np.std(vals)
    abs_val_minus_avg = abs(vals - np.mean(vals))
    max_dev = max(abs_val_minus_avg)
    max_idx = np.argmax(abs_val_minus_avg)
    esd_stat = max_dev/ stdev
    return esd_stat, max_idx

def __calculate_esd_critical_value(size, alpha=0.05):
    t_dist = t.ppf(1 - alpha / (2 * size), size - 2)
    numerator = (size - 1) * np.sqrt(np.square(t_dist))
    denominator = np.sqrt(size) * np.sqrt(size - 2 + np.square(t_dist))
    critical_value = numerator / denominator
    return critical_value

def __get_idx(idx_list,curr_idx):
    if len(idx_list)>0:
        for idx in reversed(idx_list):
            if idx <= curr_idx:
                curr_idx+=1
    return curr_idx

def generalized_esd(df, col, max_outliers=None, alpha=0.05):
    if not max_outliers:
        max_outliers = int(len(df)/2)
    col_index = column_name_to_index(df, col)
    vals = df.iloc[:,col_index].copy().to_numpy()
    rn = []
    idx_list = []
    real_idx_list = []
    for i in range(max_outliers):
        stat, max_idx = __calculate_esd_test_stat(vals)
        cv = __calculate_esd_critical_value(len(df), alpha)
        rn.append(stat>cv)
        real_idx_list.append(__get_idx(idx_list,max_idx))
        idx_list.append(max_idx)
        vals = np.delete(vals,max_idx,axis=0)
    try:
        idxs = real_idx_list[:np.max(np.where(rn)[0])+1]
    except ValueError:
        return []
    return idxs

def grubbs(df,col):
    pass # to be implemented

def dixon(df,col):
    pass # to be implemented