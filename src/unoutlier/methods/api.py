import pandas as pd
from abc import ABC, abstractmethod
from typing import List, Union
from unoutlier.methods.probabilistic import *

class AbstractUnoutlier(ABC):

    def __init__(self, method_function, **params) -> None:
        super().__init__()
        self._func = method_function 
        self._params = params
        self._idxs = []

    @abstractmethod
    def fit(self, df: pd.DataFrame, col: Union[str,List[str]]) -> None:
        pass
    
    @abstractmethod
    def clean(self, df: pd.DataFrame) -> pd.DataFrame:
        pass

    @abstractmethod
    def crop(self, df: pd.DataFrame) -> pd.DataFrame:
        pass


class UnivariateUnoutlier(AbstractUnoutlier):

    def __init__(self, method_function, **params) -> None:
        super().__init__(method_function, **params)

    def fit(self, df: pd.DataFrame, col: str) -> None:
        if col not in df.columns:
            raise AttributeError(f"Column {col} not in the dataframe")
        self._idxs = self._func(df,col,**self._params)

    def crop(self, df: pd.DataFrame) -> pd.DataFrame:
        return df.loc[self._idxs]

    def clean(self, df: pd.DataFrame) -> pd.DataFrame:
        return df.loc[df.index.difference(self._idxs),]
    

class MultivariateUnoutlier(AbstractUnoutlier):

    def __init__(self, method_function, **params) -> None:
        super().__init__(method_function, **params)

    def fit(self, df: pd.DataFrame, cols: List[str]) -> None:
        for col in cols:
            if col not in df.columns:
                raise AttributeError(f"Column {col} not in the dataframe")
        self._idxs = self._func(df,cols,**self._params)

    def crop(self, df: pd.DataFrame) -> pd.DataFrame:
        return df.loc[self._idxs]

    def clean(self, df: pd.DataFrame) -> pd.DataFrame:
        return df.loc[df.index.difference(self._idxs),]

class PyODWrapper(MultivariateUnoutlier):
    
    def __init__(self, pyod_obj, **params) -> None:
        super().__init__(pyod_obj, **params)
        self._func = self._func(**params)

    def fit(self, df: pd.DataFrame, col: Union[str,List[str]]) -> None:
        if type(col) is str:
            col = [col]
        self._func.fit(df[col])
        outliers = self._func.predict(df[col])
        self._idxs = np.where(outliers==1)[0]
