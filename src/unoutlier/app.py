import dash_bootstrap_components as dbc
import pandas as pd
import io
from dash import Dash, dcc, html, Input, Output, State, dash_table, callback_context
from base64 import b64decode
from unoutlier.utils.expert import METHODS
from unoutlier.utils.utils import sublist

app = Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

data_df = None
method_names = sorted(list(METHODS.keys()))
chosen_method = None
selected_cols = []
fname = "test.csv"

app.layout = html.Div(className="bg-dark",
    children=[
        html.H2("Unoutlier - load your data and identify outliers", 
            className="display-3 h-50 p-5 text-white bg-dark"),
        dbc.Row(
        dcc.Upload(
            id="upload-data",
            children=html.Div(["Drag and Drop or ", html.A("Select Files")],className="text-white"),
            style={
                "width": "100%",
                "height": "60px",
                "lineHeight": "60px",
                "borderWidth": "1px",
                "borderStyle": "dashed",
                "borderRadius": "5px",
                "textAlign": "center"
            },
            multiple=False,
            className="text-white"
        ), className="px-5"),
        dcc.Dropdown(placeholder="Select a method", options=method_names, id="method-dropdown", className="p-5 bg-dark"),
        html.Br(),
        html.Div(id="method-error"),
        html.Div(id="col-error"),
        html.Div(id="output-state"),
        dbc.Row(
            dcc.Checklist(id="column-checklist", inline=True, className="text-white", labelStyle={"margin-right": "10px"}, inputStyle={"margin-right": "5px"}),
            className="text-white px-5"),
        html.Div(id="output-data-upload", className="px-5"),
        html.Br(),

        html.Div([
                    dbc.Button(
                        id="submit-button-state", n_clicks=0, children="Detect outliers", className="me-1",  color="light",
                        disabled=True
                    ),
            dbc.Button(
                        id="download-button-state",
                        n_clicks=0,
                        children="Download data",
                        color="light",
                    ),
            dcc.Download(id="download-data")], className="d-grid gap-2 p-5"
            )
    ]
)

@app.callback(
    Output("col-error", "children"),
    Output("method-dropdown", "options"),
    Input("column-checklist", "value"),
    prevent_initial_call=True,
)
def update_method_based_on_cols(value):
    global selected_cols
    selected_cols = value
    return None, sorted(get_methods_by_tags(generate_tags()))


@app.callback(
    Output("method-error", "children"),
    Output("submit-button-state", "disabled"),
    Input("method-dropdown", "value"),
    prevent_initial_call=True,
)
def update_method(value):
    global chosen_method
    chosen_method = METHODS[value]
    return None, False


@app.callback(
    Output("output-data-upload", "children"),
    Output("column-checklist", "options"),
    Output("column-checklist", "value"),
    Input("upload-data", "contents"),
    State("upload-data", "filename"),
    Input("submit-button-state", "n_clicks"),
    prevent_initial_call=True,
)
def parse_content(contents, filename, nclicks):
    global data_df
    global fname 
    fname = filename
    ctx = callback_context
    component_id = ctx.triggered_id
    if component_id == "upload-data":
        type, content = contents.split(",")
        decoded_content = b64decode(content).decode("utf-8")
        if "csv" in type:
            data_df = pd.read_csv(io.StringIO(decoded_content))
        elif "xlsx" in type:
            pass
        else:
            data_df = pd.read_csv(io.StringIO(decoded_content))
        data_df["internal_idx"] = data_df.index
        return (
            dash_table.DataTable(
                id="data_tbl",
                css=[{"selector": ".show-hide", "rule": "display: none"}],
                data=data_df.to_dict("records"),
                columns=[{"name": i, "id": i} for i in data_df.columns],
                hidden_columns=["internal_idx"],
            ),
            data_df.columns.tolist()[:-1],
            data_df.columns.tolist()[:-1],
        )
    elif component_id == "submit-button-state":
        global chosen_method
        global selected_cols
        if len(selected_cols) == 1:
            cols = selected_cols[0]
        else:
            cols = selected_cols
        chosen_method["object"].fit(data_df, cols)
        rows_to_highlight = chosen_method["object"].crop(data_df).index.tolist()
        return (
            dash_table.DataTable(
                id="data_tbl",
                data=data_df.to_dict("records"),
                css=[{"selector": ".show-hide", "rule": "display: none"}],
                columns=[{"name": i, "id": i} for i in data_df.columns],
                hidden_columns=["internal_idx"],
                style_data_conditional=(
                    [
                        {
                            "if": {
                                "filter_query": f"{{internal_idx}} eq {i}",
                            },
                            "backgroundColor": "#ffc107",
                            "color": "white",
                        }
                        for i in rows_to_highlight
                    ]
                ),
            ),
            data_df.columns.tolist()[:-1],
            selected_cols,
        )

@app.callback(
    Output("download-data", "data"),
    Input("download-button-state", "n_clicks"),
    prevent_initial_call=True,
)
def create_download_file(n_clicks):
    global fname
    global data_df
    global chosen_method
    global selected_cols
    if len(selected_cols) == 1:
        cols = selected_cols[0]
    else:
        cols = selected_cols
    chosen_method["object"].fit(data_df, cols)
    original_df = data_df.loc[:, data_df.columns!='internal_idx']
    rows_select = chosen_method["object"].clean(original_df).index.tolist()
    download_df = original_df[original_df.index.isin(rows_select)].reset_index(drop=True)
    return dict(content=download_df.to_csv(index=False), filename=fname)

def get_methods_by_tags(tags):
    methods = []
    for el in METHODS:
        if sublist(tags, METHODS[el]["tags"]):
            methods.append(el)
    return methods


def generate_tags():
    global data_df
    global selected_cols
    tags = []
    for col in selected_cols:
        if not pd.to_numeric(data_df[col], errors="coerce").notnull().all():
            tags.append("categorical")
    if len(selected_cols) > 1:
        tags.append("multivariate")
    else:
        tags.append("univariate")
    return tags

if __name__ == "__main__":
    app.run_server(debug=True)
