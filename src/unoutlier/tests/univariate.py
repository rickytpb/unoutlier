import pandas as pd
from unoutlier.methods.api import UnivariateUnoutlier
from unoutlier.methods.probabilistic import (
    z_score,
    mad_score,
    tukey_iqr,
    generalized_esd,
)


df = pd.DataFrame([
    {"a": 1, "b": 2, "c": 3},
    {"a": 2, "b": 20, "c": 30},
    {"a": 3, "b": 6, "c": 3},
    {"a": 30, "b": 1, "c": 3},
    {"a": 1, "b": 6, "c": 3},
    {"a": 3, "b": 1, "c": 3},
    {"a": 4, "b": 6, "c": 3},
    {"a": 1, "b": 1, "c": 3},
])

def test_zscore():
    unoutlier = UnivariateUnoutlier(z_score,conf_level=0.05)
    unoutlier.fit(df,'a')
    outlier_df = unoutlier.crop(df)
    clean_df = unoutlier.clean(df)
    assert outlier_df['a'].values[0] == 30
    assert clean_df.shape[0] == 7

def test_madscore():
    unoutlier = UnivariateUnoutlier(mad_score,conf_level=3.5)
    unoutlier.fit(df,'a')
    outlier_df = unoutlier.crop(df)
    clean_df = unoutlier.clean(df)
    assert outlier_df['a'].values[0] == 30
    assert clean_df.shape[0] == 7

def test_iqr():
    unoutlier = UnivariateUnoutlier(tukey_iqr)
    unoutlier.fit(df,'a')
    outlier_df = unoutlier.crop(df)
    clean_df = unoutlier.clean(df)
    assert outlier_df['a'].values[0] == 30
    assert clean_df.shape[0] == 7

def test_gesd():
    unoutlier = UnivariateUnoutlier(generalized_esd)
    unoutlier.fit(df,'a')
    outlier_df = unoutlier.crop(df)
    clean_df = unoutlier.clean(df)
    assert outlier_df['a'].values[0] == 30
    assert clean_df.shape[0] == 7
