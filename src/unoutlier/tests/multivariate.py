from black import out
import pandas as pd
from unoutlier.methods.api import PyODWrapper
from unoutlier.methods.nonlinear import (
    IForest,
    AutoEncoder,
    KNN
)
from unoutlier.methods.linear import (
    OCSVM,
    PCA
)
from unoutlier.methods.distance_depth import (
    LOF
)


df = pd.DataFrame([
    {"a": 1, "b": 2, "c": 3},
    {"a": 2, "b": 20, "c": 30},
    {"a": 3, "b": 6, "c": 3},
    {"a": 30, "b": 1, "c": 3},
    {"a": 1, "b": 6, "c": 3},
    {"a": 3, "b": 1, "c": 3},
    {"a": 4, "b": 6, "c": 3},
    {"a": 1, "b": 1, "c": 3},
])

def test_iforest():
    unoutlier = PyODWrapper(IForest)
    unoutlier.fit(df,'a')
    outlier_df = unoutlier.crop(df)
    clean_df = unoutlier.clean(df)
    assert outlier_df['a'].values[0] == 30
    assert clean_df.shape[0] == 7
    unoutlier.fit(df,df.columns)
    outlier_df = unoutlier.crop(df)
    clean_df = unoutlier.clean(df)
    assert outlier_df['a'].values[0] != 30
    assert clean_df.shape[0] == 7

def test_knn():
    unoutlier = PyODWrapper(KNN)
    unoutlier.fit(df,'a')
    outlier_df = unoutlier.crop(df)
    clean_df = unoutlier.clean(df)
    assert outlier_df['a'].values[0] == 30
    assert clean_df.shape[0] == 7

def test_autoencoder():
    unoutlier = PyODWrapper(AutoEncoder, hidden_neurons=[1,1])
    unoutlier.fit(df,'a')
    outlier_df = unoutlier.crop(df)
    clean_df = unoutlier.clean(df)
    assert outlier_df['a'].values[0] == 30
    assert clean_df.shape[0] == 7

def test_pca():
    unoutlier = PyODWrapper(PCA)
    unoutlier.fit(df,'a')
    outlier_df = unoutlier.crop(df)
    clean_df = unoutlier.clean(df)
    assert outlier_df['a'].values[0] == 30
    assert clean_df.shape[0] == 7

def test_ocsvm():
    unoutlier = PyODWrapper(OCSVM)
    unoutlier.fit(df,'a')
    outlier_df = unoutlier.crop(df)
    clean_df = unoutlier.clean(df)
    assert outlier_df['a'].values[0] == 30
    assert clean_df.shape[0] == 7

def test_lof():
    unoutlier = PyODWrapper(LOF)
    unoutlier.fit(df,df.columns)
    outlier_df = unoutlier.crop(df)
    assert outlier_df.shape[0] == 0

if __name__ == "__main__":
    test_lof()