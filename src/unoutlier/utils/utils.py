import pandas as pd
from typing import Union

def column_name_to_index(df: pd.DataFrame, col: str) -> Union[str, pd.DataFrame]:
    if isinstance(col,int):
        return col
    if isinstance(col,str):
        return df.columns.get_loc(col)
    raise AttributeError("Provide column name or index")

def sublist(lst1, lst2):
    return set(lst1) <= set(lst2)
