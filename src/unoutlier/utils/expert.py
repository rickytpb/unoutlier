from unoutlier.methods.api import PyODWrapper
from unoutlier.methods.nonlinear import IForest, AutoEncoder, KNN
from unoutlier.methods.linear import OCSVM, PCA
from unoutlier.methods.distance_depth import LOF, HBOS
from unoutlier.methods.api import UnivariateUnoutlier
from unoutlier.methods.probabilistic import (
    z_score,
    mad_score,
    tukey_iqr,
    generalized_esd,
)

METHODS = {
    "Z-score": {
        "tags": ["univariate", "probabilistic"],
        "object": UnivariateUnoutlier(z_score, conf_level=0.05),
    },
    "MAD-score": {
        "tags": ["univariate", "probabilistic"],
        "object": UnivariateUnoutlier(mad_score, conf_level=3.5),
    },
    "IQR (Tukey)": {
        "tags": ["univariate", "probabilistic"],
        "object": UnivariateUnoutlier(tukey_iqr),
    },
    "GESD": {
        "tags": ["univariate", "probabilistic"],
        "object": UnivariateUnoutlier(generalized_esd),
    },
    "One-class SVM": {"tags": ["multivariate", "linear"], "object": PyODWrapper(OCSVM)},
    "PCA": {"tags": ["multivariate", "linear"], "object": PyODWrapper(PCA)},
    "Isolation Forest": {
        "tags": ["multivariate", "non-linear", "categorical"],
        "object": PyODWrapper(IForest),
    },
    "KNN": {
        "tags": [
            "multivariate",
            "non-linear",
        ],
        "object": PyODWrapper(KNN),
    },
    "Autoencoder": {
        "tags": ["multivariate", "non-linear", "categorical"],
        "object": PyODWrapper(AutoEncoder, hidden_neurons=[5, 5]),
    },
    "LOF ": {"tags": ["multivariate", "density"], "object": PyODWrapper(LOF)},
    "ABOD ": {"tags": ["multivariate", "density"], "object": PyODWrapper(HBOS)}
}
